# Campaign Manager #

The Campaign Manager Android App was first demoed at the SharePoint Conference 2014 keynote. Watch the [keynote](http://channel9.msdn.com/Events/SharePoint-Conference/2014/KEY01) at Channel 9.

**Table of Contents**
- [Overview](#overview)
- [Quick Start](#quick-start)
- [Environment Setup](#environment-setup)
- [Related Resource](#related-resource)

## Overview ##
The Campaign Manager Android App was built to demonstrate the power of the new Office 365 APIs which allow developers to access Office 365 services such as OneDrive for Business, Mail, Calendar, Sites and Users & Groups.

## Quick start ##
If you just wish to demo the Android App without pulling the code and compiling, you can just grab the [APK file](https://github.com/OfficeDev/Campaign-Manager-Code-Sample/tree/master/AppPackage) and upload it to your Android Device. There is a configuration step when you launch the app to provide all your environmental information for Office 365.

## Environment Setup ##
We understand a fair few of you may not have done Android development before, so we have provided a [set up guide](https://github.com/OfficeDev/Campaign-Manager-Code-Sample/blob/master/Documents/IDE%20Setup%20Instructions.txt) to get you on your way with Eclipse.

## Related Resource ##
- [Developer Center](http://dev.office.com/)  
- [Office 365 Developer Blog](http://blogs.office.com/dev)
- [Office 365 API documentatoin] (http://msdn.microsoft.com/en-us/library/office/dn605892(v=office.15).aspx)
